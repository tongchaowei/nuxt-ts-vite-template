# Nuxt 3 + TypeScript + Vite 初始项目模板

本项目基于 Nuxt 3 + TypeScript + Vite 创建的初始项目模板，其中包含了一些常用的配置和工具，方便开发。

## 目前已完成配置的内容

- Vite
- TypeScript
- ESLint
- vite-plugin-eslint
  - 实现 Vite 运行或构建项目时，能够自动执行 ESLint 代码检查和修复
- Prettier
- Sass
- Stylelint
- husky
- pnpm
  - 已通过配置实现统一项目中使用的安装依赖的包管理工具为 pnpm

## 本项目的搭建文档

- [Nuxt 3 + TypeScript + Vite 项目模板搭建（TypeScript、ESLint、Prettier、Sass、Stylelint、husky、Vite、pnpm 配置）](https://www.yuque.com/u27599042/eikcfe/nzlfuq9636phrgcg)

## 本项目搭建过程中的报错及其解决方法

- [Vite 项目中配置 vite-plugin-eslint 插件报错 Could not find a declaration file for module vite-plugin-eslint. implicitly has an any type.](https://www.yuque.com/u27599042/vue/cgnesb8kh65dpu4s)

## 项目依赖安装

```bash
pnpm i
```

## 项目 package.json 中脚本说明

```bash
"preinstall": "node ./scripts/preinstall.js"
```

使用包管理工具安装依赖时，该脚本会自动执行，会检查当前使用的安装依赖的包管理工具是否为 pnpm，如果不是，则会停止安装依赖并提示用户使用 pnpm 安装依赖。

```bash
"lint": "eslint ./src"
```

执行该脚本，会执行 ESLint 对 src 目录下文件中的代码进行检查

```bash
"fix": "eslint ./src --fix"
```

执行该脚本，会执行 ESLint 对 src 目录下文件中的代码进行检查和修复

```bash
"format": "prettier --write \"./src/**/*.{html,vue,ts,js,json,md}\" --config ./.prettierrc.json"
```

执行该脚本，会执行 Prettier 对 src 目录下 html,vue,ts,js,json,md 文件中的代码进行格式化

```bash
"lint:eslint": "eslint ./src/**/*.{js,jsx,ts,tsx,vue} --cache --fix"
```

执行该脚本，会执行 ESLint 对 src 目录下 js,jsx,ts,tsx,vue 文件中的代码进行检查和修复

```bash
"lint:style": "stylelint ./src/**/*.{css,scss,vue} --cache --fix"
```

执行该脚本，会执行 Stylelint 对 src 目录下 css,scss,vue 文件中的代码进行检查和修复

```bash
"dev": "nuxt dev --open --dotenv .env.dev"
```

执行该脚本，会启动一个本地服务器，并自动打开浏览器访问该服务器，此时的环境模式为 dev 开发模式

```bash
"test": "nuxt dev --open --dotenv .env.test"
```

执行该脚本，会启动一个本地服务器，并自动打开浏览器访问该服务器，此时的环境模式为 text 测试模式

```bash
"build": "nuxt build --dotenv .env.prod"
```

对项目进行构建打包，构建打包结果在项目根目录的 .output 目录中，此时的环境模式为 prod 生产模式

```bash
"preview": "nuxt preview --open"
```

对项目构建结果进行预览，执行该脚本，会启动一个本地服务器，并自动打开浏览器访问该服务器，此时的环境模式为 prod 生产模式

```bash
"generate": "nuxt generate --dotenv .env.prod"
```

执行该脚本，生成项目对应的静态网页，此时的环境模式为 prod 生产模式

```bash
"postinstall": "nuxt prepare"
```

执行该脚本，在项目中安装 Nuxt 相关的类型说明文件，避免 TS 类型找不到而报错
